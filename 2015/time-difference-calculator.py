#!/usr/bin/env python3

# Get start and end times
startTime = input("Enter start time [h:m:s]> ")
endTime = input("Enter end time [h:m:s]> ")

# Convert values to array with numbers [h,m,s]
startTime = startTime.split(':')
endTime = endTime.split(':')

def validateTimes():
        # Check that corrent number of inputs were given
        if len(startTime) != 3 or len(endTime) != 3:
            return False

        # Check all values are not empty
        for i in range(3):
            if startTime[i] == '' or endTime[i] == '':
                return False
        
        return True

if not validateTimes():
    print("Invalid time/s input")
    exit()

# Convert each array value to int
def convertToInts(array):
    i = 0
    for timeValue in array:
        array[i] = int(timeValue)
        i+=1

convertToInts(startTime)
convertToInts(endTime)

output = [0,0,0]
for i in range(3):
    output[i] = endTime[i] - startTime[i]
    if endTime[i] < startTime[i]:
        output[i-1] -= 1
        output[i] += 60

if output[0] < 0: 
    outputText = "You finished before you began."
else:
    outputText = "Time taken was "
    start = True
    for i in output:
        if not start:
            outputText += ":"
        outputText += str(i)
        start = False
    
print(outputText)