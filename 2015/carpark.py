#!/usr/bin/env python3

# Get dimensions of the car park
width = int(input("Enter the carpark width: "))
length = int(input("Enter the carpark length: "))

# Check they are postitive and even
if width <= 0 or width % 2 == 1 or  length <= 0 or length % 2 == 1:
    print("Invalid dimensions, must be even and positive.")
    exit()

# Render carpark
def render():
    for i in range(length):
        row = ""
        for j in range(width):
            row += "+-----"
        row += "+"
        print(row)

        for k in range(2):
            row = ""
            for j in range(width):
                row += "|     "
            row += "|"
            print(row)

    row = ""
    for j in range(width):
        row += "+-----"
    row += "+"
    print(row)

render()